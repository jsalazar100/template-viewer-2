import {handler} from '../app/scripts/handler.component'
import {expect} from 'chai'
import fs from 'fs'
import jsdom from 'jsdom'

const { JSDOM } = jsdom;
const mockHTML = fs.readFileSync('test/mockHandler.html', 'utf-8');

describe('handler.component', () => {
  
  describe('handler()', () => {

    // set up mocks
    beforeEach(() => {

      // Mock HTML Container
      const dom = new JSDOM(mockHTML, {url: 'http://localhost', runScripts: "dangerously"});

      global.window = dom.window;
      global.document = dom.window.document;
     
    }); // ends beforeEach


    it('NEXT BTN: should change current visible index--show next container if next is clicked', () => {
      // load all handler() functionality
      handler();
      
      // should show large photo and details onClick
      // mock click of thumbnail to show largePhoto

      const thumbGroupsContainer = document.querySelectorAll('.thumbGroup');
      let currentIndex = 0;
      
      // get current index of visible thumbnail container
      Array.from(thumbGroupsContainer).forEach((group, current) => {
        let currentGroup = document.getElementById(group.id);
        if (!currentGroup.classList.contains('group-hide')) {
            currentIndex = current;
        } 
      });

      // handler() is loaded, so simulate click of the NEXT button against it
      document.querySelector("#next").dispatchEvent(new MouseEvent('click', {bubbles: true}))
      
      // confirm handler is working as expected
      let newIndex = 0;
      
      // get new index of visible thumbnail container
      Array.from(thumbGroupsContainer).forEach((group, current) => {
        let currentGroup = document.getElementById(group.id);
        if (!currentGroup.classList.contains('group-hide')) {
            newIndex = current;
        } 
      });
      
      console.log(currentIndex +' -- '+ newIndex)
      // if handler() works, we should have a new index
      expect(currentIndex == newIndex).to.be.false;
    });


    it('NEXT BTN: should stay on last thumbnail container if end is reached', () => {
      // load all handler() functionality
      handler();

      const thumbGroupsContainer = document.querySelectorAll('.thumbGroup');
      const thumbContainerLength = thumbGroupsContainer.length;
      // simulate viewing of last container
      let currentIndex = thumbContainerLength - 1;
      // assuming onLoad: first container visible by design, so
      // hide first container
      thumbGroupsContainer[0].classList.add('group-hide');
      // show last container
      thumbGroupsContainer[currentIndex].classList.remove('group-hide');

      // handler() is loaded, so simulate click of the NEXT button against it
      document.querySelector("#next").dispatchEvent(new MouseEvent('click', {bubbles: true}))
      
      // confirm handler is working as expected
      let newIndex = 0;
      
      // get new index of visible thumbnail container
      Array.from(thumbGroupsContainer).forEach((group, current) => {
        let currentGroup = document.getElementById(group.id);
        if (!currentGroup.classList.contains('group-hide')) {
            newIndex = current;
        } 
      });
      
      console.log(currentIndex +' -- '+ newIndex)
      // if handler() works, we should same index
      expect(currentIndex == newIndex).to.be.true;
    });


    it('PREVIOUS BTN: should change current visible index--show previous container if previous is clicked', () => {
      // load all handler() functionality
      handler();
      
      const thumbGroupsContainer = document.querySelectorAll('.thumbGroup');
      const thumbContainerLength = thumbGroupsContainer.length;
      // simulate viewing of last container
      let currentIndex = thumbContainerLength - 1;
      // assuming onLoad: first container visible by design, so
      // hide first container
      thumbGroupsContainer[0].classList.add('group-hide');
      // simluate viewing of last container
      thumbGroupsContainer[currentIndex].classList.remove('group-hide');

      // handler() is loaded, so simulate click of the PREVIOUS button against it
      document.querySelector("#previous").dispatchEvent(new MouseEvent('click', {bubbles: true}))
      
      // confirm handler is working as expected
      let newIndex = 0;
      
      // get new index of visible thumbnail container
      Array.from(thumbGroupsContainer).forEach((group, current) => {
        let currentGroup = document.getElementById(group.id);
        if (!currentGroup.classList.contains('group-hide')) {
            newIndex = current;
        } 
      });

      console.log(currentIndex +' -- '+ newIndex)
      // if handler() works, we should have a new index
      expect(currentIndex == newIndex).to.be.false;
    });


    it('PREVIOUS BTN: should stay on first thumbnail container if beginning is reached', () => {
      // load all handler() functionality
      handler();

      const thumbGroupsContainer = document.querySelectorAll('.thumbGroup');
      // simulate viewing of first container
      // assuming onLoad: first container visible by design, so
      let currentIndex = 0;

      // handler() is loaded, so simulate click of the PREVIOUS button against it
      document.querySelector("#previous").dispatchEvent(new MouseEvent('click', {bubbles: true}))
      
      // confirm handler is working as expected
      let newIndex = 0;
      
      // get new index of visible thumbnail container
      Array.from(thumbGroupsContainer).forEach((group, current) => {
        let currentGroup = document.getElementById(group.id);
        if (!currentGroup.classList.contains('group-hide')) {
            newIndex = current;
        } 
      });
      
      console.log(currentIndex +' -- '+ newIndex)
      // if handler() works, we should same index
      expect(currentIndex == newIndex).to.be.true;
    });


    it('THUMBNAIL IMG LINK: should change large photo/details displayed', () => {
      // load all handler() functionality
      handler();
      
      // get id of currently visible large photo
      // the 1st photo is visible by design, so..
      const currentPhotoID = document.querySelector('.largeGroup').id;
    
      // handler() is loaded, so simulate click of the THUMBNAIL against it
      // simulate clicking on 2nd thumbnail, 1st one is viable by design
      document.querySelector("#tG0 > a:nth-child(2) > img").dispatchEvent(new MouseEvent('click', {bubbles: true}))
      
      // confirm handler is working as expected
      let newPhotoID = 0;
      
      // get new id of visible large photo
      const photoContainers = document.querySelectorAll('.largeGroup');
      Array.from(photoContainers).forEach((photo) => {
        let currentPhoto = document.getElementById(photo.id);
        if (!currentPhoto.classList.contains('group-hide')) {
            newPhotoID = photo.id;
        } 
      });
      
      console.log(currentPhotoID +' -- '+ newPhotoID)
      // if handler() works, we should have a new photo id
      expect(currentPhotoID == newPhotoID).to.be.false;
    });


  });

});