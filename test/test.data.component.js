import {data} from '../app/scripts/data.component'
// import {photos} from '../app/scripts/photo.component'
// import {thumbnails} from '../app/scripts/thumbnails.component'
// import {handler} from '../app/scripts/handler.component'
// const jsonData = require('../app/data/template.json')
import fs from 'fs'
import {expect} from 'chai';
global.fetch = require("node-fetch");
import jsdom from 'jsdom'

const { JSDOM } = jsdom;
const mockHTML = fs.readFileSync('test/mockData.html', 'utf-8');


describe('data.component', () => {
  
    describe('data()', () => {
  
      // set up mocks
      beforeEach(() => {
  
        // Mock HTML Container
        const dom = new JSDOM(mockHTML, {url: 'http://localhost', runScripts: "dangerously"});

        global.window = dom.window;
        global.document = dom.window.document;

        const dataURL = 'http://localhost:9000/data/template.json';
        data(dataURL);
        
        const largeGroups = global.document.querySelectorAll('.largeGroup');
        global.largeGroups = largeGroups;
              
      }); // ends beforeEach
  
  
      it('should run fetch with http response code of 200');
      it('should run two functions: thumbnails(), photos()');
      it('should check if thumbnailsContainers length gt(0)');
      it('should check if photoContainers length gt(0)');
      it('should return specific JSON properties (title, cost, id, etc)');
    
    });
  
  });