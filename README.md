# README.md

## DEMO

http://jakesalazar.com/demo/template-viewer-2


## Installation

Clone this repo:

`git clone git@bitbucket.org:jsalazar100/template-viewer-2.git`

CD into the project directory, then:

`$ npm install`

To start the server and begin development:

`$ npm run start`

To build for production:

`$ npm run build`

To run unit tests:

`$ npm run test`

----


## Version 2 Assumptions
* I had a little more time and a better understanding of the requirements. 
* I decided to build a more robust project and pay more attention to structure, details, and developer experience.

## Version 2 Approach
* I used a front-end tool called [Yeoman](https://yeoman.io/) paired with scaffolding provided by [generator-webapp](https://github.com/yeoman/generator-webapp).
* I refactored my previous POJO code with ES modules.
* I still elected not to used a Javascript library/framework. 
* I used the Bootstrap CSS, but did not load or use jQuery or Bootstrap javascript.
* I split up my ES modules into 4 components for maintainability: 
    * data.component.js
    * handler.component.js
    * photo.component.js
    * thumbnails.component.js
    * and main.js to start the party

  The data() component fetches data and calls photo() and thumbnails() components, which do the heavy lifting creating our DOM.
  The handler() component controls all clicks with a single eventListerner.

* If your curious about how I did this, please see my notes below for this setup.

* I attempted a quick optimize for mobile.

* For this SPA, I created a skeleton container from the HTML provided (using Bootstrap) and then hydrated it with a data fetch onLoad.



## Changes to HTML/CSS
* Generator-webapp provides tons of functionality baked in. 
* So, I kept most of the scaffolding provided (files/directories) with some tweaks.
* But, I made changes to Gulp to support ES modules usage for both app architecture and testing.


----

# Developer Setup: Yeoman, Generator-Webapp, Gulp, etc
- I had to set up [Browserify](https://github.com/browserify/browserify) with [Babelify](https://github.com/babel/babelify) with [Gulp](https://gulpjs.com/) so I could use ES modules (import/export) to support the app architecture I wanted.
- As a result of this, I also had to set up [ESM](https://github.com/standard-things/esm) with [Mocha](https://mochajs.org/), so I could test against native ES modules.
- I also set up [JSDOM](https://github.com/jsdom/jsdom) with Mocha to test DOM manipulations.
- Just for funzies, I also learned how to use Mocha and Chai for this, so that was cool.


