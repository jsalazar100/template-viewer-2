import {thumbnails} from './thumbnails.component'
import {photos} from './photo.component'
// this is our smart component
// it is loaded by main, then
// fetches and passes our data to two functions that build our dom
export function data(dataURL) {
    fetch(dataURL)
        .then((response) => {
            if (!response.ok) {
                throw new Error('Network response was not ok');
            }
            return response.json();
        })
        .then((data) => {
            thumbnails(data);
            photos(data);
        })
        .catch((error) => {
            console.error('Fetch Error: ' + error);
        });
}