export function photos(data) {
    let largeContainer = document.getElementById('largeRoot');

    for (let i = 0; i < data.length; i++) {
        // create large photo containers from our data
        let largeGroup = document.createElement('div');
            largeGroup.id = data[i].id;
            largeGroup.classList.add('largeGroup');
            largeGroup.classList.add('row');
            // hide all large photos except 1st one onLoad
            if (i !== 0) {
                largeGroup.classList.add('group-hide');
            }
            largeGroup.innerHTML = `
            <div class="col-7 largePhoto"><img src="images/large/${data[i].image}" alt="${data[i].title}" /></div>
            <div class="col-5 details">
                <p><strong>Title</strong> ${data[i].title}</p>
                <p><strong>Description</strong>  ${data[i].description}</p>
                <p><strong>Cost</strong> ${data[i].cost}</p>
                <p><strong>ID #</strong> ${data[i].id}</p>
                <p><strong>Thumbnail File</strong> ${data[i].thumbnail}</p>
                <p><strong>Large Image File</strong> ${data[i].image}</p>
            </div>
            `;
        // add each photo container to its root parent
        // not exactly performant, but it will do for now
        // it might be faster if we perform a single append of all children
        largeContainer.appendChild(largeGroup);
    }
}